import {
  CPlugin,
  CPluginClient,
} from "@bettercorp/service-base/lib/interfaces/plugins";
import { MyPluginConfig } from "./sec.config";
import * as LRU from "lru-cache";
const CRON_JOB = require("cron").CronJob;

export class lruCache<T = any> extends CPluginClient<MyPluginConfig> {
  public readonly _pluginName: string = "lru-cache";

  async reset(): Promise<void> {
    return await this.emitEvent("reset");
  }

  async del(key: string): Promise<void> {
    return await this.emitEvent("del", key);
  }

  async load<DataType = T>(
    cacheEntriesArray: [string, LRU.Entry<DataType>][]
  ): Promise<void> {
    return await this.emitEvent("load", cacheEntriesArray);
  }

  async prune(): Promise<void> {
    return await this.emitEvent("reset");
  }

  set(key: string, value: any, maxAge: number): Promise<boolean> {
    return this.emitEventAndReturn<any, boolean>("set", {
      key,
      value,
      maxAge,
    });
  }

  get<DataType = T>(key: string): Promise<DataType> {
    return this.emitEventAndReturn<any, DataType>("get", {
      key,
    });
  }

  peek<DataType = T>(key: string): Promise<DataType> {
    return this.emitEventAndReturn<any, DataType>("peek", {
      key,
    });
  }

  has(key: string): Promise<boolean> {
    return this.emitEventAndReturn<any, boolean>("has", {
      key,
    });
  }

  keys(): Promise<Array<string>> {
    return this.emitEventAndReturn<any, Array<string>>("keys");
  }

  values<DataType = T>() {
    return this.emitEventAndReturn<any, Array<DataType>>("values");
  }

  dump<DataType = T>(): Promise<Array<LRU.Entry<DataType>>> {
    return this.emitEventAndReturn<any, Array<LRU.Entry<DataType>>>("dump");
  }
}

export class Plugin extends CPlugin<MyPluginConfig> {
  private _cache!: LRU<string, any>;

  init(): Promise<void> {
    const self = this;
    return new Promise(async (resolve) => {
      self._cache = new LRU({
        max: 500,
        length: function (n: number, key: string) {
          return n * 2 + key.length;
        },
        maxAge: 1000 * 60 * 60,
      });
      self._cache.clear();

      self.onEvent(null, "reset", async () => self.reset());
      self.onEvent(null, "prune", async () => self.prune() as any);
      self.onEvent(null, "del", async (key: string) => self.del(key) as any);
      self.onEvent(null, "load", async (cacheEntriesArray: Array<any>) =>
        self.load(cacheEntriesArray)
      );
      self.onReturnableEvent(null, "set", (d: any) =>
        self.set(d.key, d.value, d.mavAge)
      );
      self.onReturnableEvent(null, "get", (d: any) => self.get(d.key));
      self.onReturnableEvent(null, "peek", (d: any) => self.peek(d.key));
      self.onReturnableEvent(null, "has", (d: any) => self.has(d.key));
      self.onReturnableEvent(null, "keys", () => self.keys());
      self.onReturnableEvent(null, "values", () => self.values());
      self.onReturnableEvent(null, "dump", () => self.dump());
      resolve();
    });
  }

  loaded(): Promise<void> {
    const self = this;
    return new Promise((resolve) => {
      setInterval(() => {
        self._cache.purgeStale();
      }, 60 * 60 * 1000);
      new CRON_JOB(
        "0 0 * * *",
        () => {
          self._cache.clear();
        },
        null,
        true,
        "Africa/Harare"
      );
      self.emitEvent("another-plugin-name", "another-plugin-on-event", "0");
      resolve();
    });
  }

  reset() {
    return this._cache.clear();
  }

  set(key: string, value: any, maxAge: number) {
    let self = this;
    return new Promise((resolve, reject) => {
      try {
        let isString = true;
        if (typeof value !== "string") {
          value = "<~>" + JSON.stringify(value);
          isString = false;
        }
        resolve(
          self._cache.set(key, isString ? JSON.stringify(value) : value, {
            ttl: maxAge,
          })
        );
      } catch (Exc) {
        reject(Exc);
      }
    });
  }

  get(key: string) {
    let self = this;
    return new Promise((resolve, reject) => {
      try {
        let val = self._cache.get(key);
        if (val !== undefined && val !== null && val.indexOf("<~>") === 0) {
          val = JSON.parse(val.substr(3));
        }
        resolve(val);
      } catch (Exc) {
        reject(Exc);
      }
    });
  }

  peek(key: string) {
    let self = this;
    return new Promise((resolve, reject) => {
      try {
        let val = self._cache.peek(key);
        if (val !== undefined && val !== null && val.indexOf("<~>") === 0)
          val = JSON.parse(val.substr(3));
        resolve(val);
      } catch (Exc) {
        reject(Exc);
      }
    });
  }

  del(key: string) {
    return this._cache.del(key);
  }

  //delLike(key: string, rechecked: boolean = false) {
  //this._cache.reset();
  /*console.log(`[CACHE] DEL LIKE ${key} (${rechecked})`);
  for (let _key of this.keys()) {
    if (_key.indexOf(key) === 0) {
      console.log(`[CACHE] DEL LIKE ${key} : ${_key}`);
      this._cache.del(key);
    } else {
      console.log(`[CACHE] NO DEL : ${_key}`);
    }
  }
  this._cache.prune();
  console.log(`[CACHE] DEL LIKE ${key} : DONE`);
  if (!rechecked)
    this.delLike(key, true);*/
  //}

  has(key: string) {
    let self = this;
    return new Promise((resolve, reject) => {
      try {
        resolve(self._cache.has(key));
      } catch (Exc) {
        reject(Exc);
      }
    });
  }

  keys() {
    let self = this;
    return new Promise((resolve, reject) => {
      try {
        resolve(self._cache.keys());
      } catch (Exc) {
        reject(Exc);
      }
    });
  }

  values() {
    let self = this;
    return new Promise((resolve, reject) => {
      try {
        resolve(self._cache.values());
      } catch (Exc) {
        reject(Exc);
      }
    });
  }

  dump() {
    let self = this;
    return new Promise((resolve, reject) => {
      try {
        resolve(self._cache.dump());
      } catch (Exc) {
        reject(Exc);
      }
    });
  }

  load(cacheEntriesArray: [string, LRU.Entry<any>][]) {
    return this._cache.load(cacheEntriesArray);
  }

  prune() {
    return this._cache.purgeStale();
  }
}
