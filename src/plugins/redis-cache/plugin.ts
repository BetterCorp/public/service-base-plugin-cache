import {
  CPlugin,
  CPluginClient,
} from "@bettercorp/service-base/lib/interfaces/plugins";
import { MyPluginConfig } from "./sec.config";
import { Redis, ReJSONGetParameters } from "redis-modules-sdk";
import { Tools } from "@bettercorp/tools/lib/Tools";

export class cache<DataType = any> extends CPluginClient<MyPluginConfig> {
  public readonly _pluginName: string = "redis-cache";
  public uSelf: CPlugin;
  constructor(self: CPlugin) {
    super(self);
    this.uSelf = self;
  }

  public search<T extends CacheItem>(
    key: string,
    timeSeconds?: number
  ): cacheSearch<T> {
    return new cacheSearch<T>(this, key, timeSeconds);
  }

  async get<T = DataType>(
    id: string,
    path: string = ".",
    params?: ReJSONGetParameters
  ): Promise<T | null> {
    return this.emitEventAndReturn("get", {
      id,
      path,
      params,
    });
  }
  async set<T = DataType>(
    id: string,
    path: string = ".",
    data: T
  ): Promise<void> {
    return this.emitEventAndReturn("set", {
      id,
      path,
      data,
    });
  }
  async touch(id: string, timeSeconds: number): Promise<void> {
    return this.emitEventAndReturn("touch", {
      id,
      timeSeconds,
    });
  }
  async del(id: string, path: string = "."): Promise<void> {
    return this.emitEventAndReturn("del", {
      id,
      path,
    });
  }
}

export class cacheSearch<T extends CacheItem> {
  private uSelf: cache;
  private searchKey: string;
  private timeout?: number;
  constructor(uSelf: cache, key: string, timeout?: number) {
    this.uSelf = uSelf;
    this.searchKey = key;
    this.timeout = timeout;
  }

  async setup(
    uniqueKeyPath: string,
    uniqueKeyAs: string,
    keys: Array<SearchKeys>
  ): Promise<void> {
    return this.uSelf.emitEventAndReturn<
      {
        pluginName: string;
        key: string;
        uniqueKeyPath: string;
        uniqueKeyAs: string;
        keys: Array<SearchKeys>;
        touchTimeout?: number;
      },
      void
    >("setup-search", {
      pluginName: this.uSelf.uSelf.pluginName,
      key: this.searchKey,
      uniqueKeyPath,
      uniqueKeyAs,
      keys,
      touchTimeout: this.timeout,
    });
  }
  async search(data: any, touch: boolean): Promise<Array<T>> {
    return this.uSelf.emitEventAndReturn<
      {
        pluginName: string;
        key: string;
        data: any;
        touch: boolean;
      },
      Array<T>
    >("do-search", {
      pluginName: this.uSelf.uSelf.pluginName,
      key: this.searchKey,
      data,
      touch,
    });
  }
  async set(data: CacheItem): Promise<void> {
    return this.uSelf.emitEventAndReturn<
      {
        pluginName: string;
        key: string;
        data: CacheItem;
      },
      void
    >("set-search", {
      pluginName: this.uSelf.uSelf.pluginName,
      key: this.searchKey,
      data,
    });
  }
  async del(id: string): Promise<void> {
    return this.uSelf.emitEventAndReturn<
      {
        pluginName: string;
        key: string;
        id: string;
      },
      void
    >("del-search", {
      pluginName: this.uSelf.uSelf.pluginName,
      key: this.searchKey,
      id,
    });
  }
  async touch(id: string): Promise<void> {
    return this.uSelf.emitEventAndReturn<
      {
        pluginName: string;
        key: string;
        id: string;
      },
      void
    >("touch-search", {
      pluginName: this.uSelf.uSelf.pluginName,
      key: this.searchKey,
      id,
    });
  }
  async get(id: string): Promise<T | null> {
    return this.uSelf.emitEventAndReturn<
      {
        pluginName: string;
        key: string;
        id: string;
      },
      T | null
    >("get-search", {
      pluginName: this.uSelf.uSelf.pluginName,
      key: this.searchKey,
      id,
    });
  }
}

export class Plugin extends CPlugin<MyPluginConfig> {
  public Redis!: Redis;

  init(): Promise<void> {
    const self = this;
    return new Promise(async (resolve) => {
      self.log.info(
        `Setup DB[REDIS] ${(await self.getPluginConfig()).db.host}/${
          (await self.getPluginConfig()).db.db
        }`
      );
      self.Redis = new Redis((await self.getPluginConfig()).db);
      self.log.info(
        `Connect DB[REDIS] ${(await self.getPluginConfig()).db.host}/${
          (await self.getPluginConfig()).db.db
        }`
      );
      await self.Redis.connect();
      self.log.info(`Ready DB[REDIS]`);

      self.onReturnableEvent(null, "get", (d: any) =>
        self.get(d.id, d.path, d.params)
      );
      self.onReturnableEvent(null, "set", (d: any) =>
        self.set(d.id, d.path, d.data)
      );
      self.onReturnableEvent(null, "touch", (d: any) =>
        self.touch(d.id, d.timeSeconds)
      );
      self.onReturnableEvent(null, "del", (d: any) => self.del(d.id, d.path));
      self.onReturnableEvent<
        {
          pluginName: string;
          key: string;
          uniqueKeyPath: string;
          uniqueKeyAs: string;
          keys: Array<SearchKeys>;
        },
        void
      >(null, "setup-search", (d: any) =>
        self.setupSearch(
          d.pluginName,
          d.key,
          d.uniqueKeyPath,
          d.uniqueKey,
          d.keys
        )
      );
      self.onReturnableEvent<
        {
          pluginName: string;
          key: string;
          data: any;
          touch: boolean;
        },
        Array<CacheItem>
      >(null, "do-search", (d: any) =>
        self.doSearch(d.pluginName, d.key, d.data, d.touch)
      );
      self.onReturnableEvent<
        {
          pluginName: string;
          key: string;
          data: any;
        },
        void
      >(null, "set-search", (d: any) =>
        self.addSearchData(d.pluginName, d.key, d.data)
      );
      self.onReturnableEvent<
        {
          pluginName: string;
          key: string;
          id: string;
        },
        void
      >(null, "del-search", (d: any) =>
        self.delSearchData(d.pluginName, d.key, d.data)
      );
      self.onReturnableEvent<
        {
          pluginName: string;
          key: string;
          id: string;
        },
        void
      >(null, "touch-search", (d: any) =>
        self.touchSearchData(d.pluginName, d.key, d.data)
      );
      self.onReturnableEvent<
        {
          pluginName: string;
          key: string;
          id: string;
        },
        CacheItem | null
      >(null, "get-search", (d: any) =>
        self.getSearchData(d.pluginName, d.key, d.data)
      );
      resolve();
    });
  }

  get<T = any>(
    id: string,
    path: string = ".",
    params?: ReJSONGetParameters
  ): Promise<T | null> {
    const self = this;
    return new Promise((resolve, reject) => {
      self.log.debug(`GET[${id}${path}]`);
      self.Redis.rejson_module_get(id, path, params)
        .then((x) => {
          if (!Tools.isNullOrUndefined(x) && x !== "") resolve(JSON.parse(x));
          else resolve(null);
        })
        .catch(reject);
    });
  }
  set<T = any>(
    id: string,
    path: string = ".",
    data: T,
    timeSeconds?: number
  ): Promise<void> {
    const self = this;
    return new Promise((resolve, reject) => {
      self.log.debug(`SET[${id}${path}]`);
      self.Redis.rejson_module_set(id, path, JSON.stringify(data))
        .then(async (x) => {
          if (x === "OK") {
            if (Tools.isNumber(timeSeconds) && timeSeconds! > 0)
              await self.touch(id, timeSeconds!);
            return resolve();
          }
          reject(x);
        })
        .catch(reject);
    });
  }
  touch(id: string, timeSeconds: number): Promise<void> {
    const self = this;
    return new Promise((resolve, reject) => {
      self.log.debug(`TOUCH[${id}${timeSeconds}]`);
      self.Redis.sendCommand({
        command: "EXPIRE",
        args: [id, timeSeconds],
      })
        .then(resolve)
        .catch(reject);
    });
  }
  del(id: string, path: string = "."): Promise<void> {
    const self = this;
    return new Promise((resolve, reject) => {
      self.log.debug(`DEL[${id}${path}]`);
      self.Redis.rejson_module_del(id, path)
        .then(() => resolve())
        .catch(reject);
    });
  }

  // search stuffs
  async setupSearch(
    pluginName: string,
    key: string,
    uniqueKeyPath: string,
    uniqueKeyAs: string,
    keys: Array<SearchKeys>,
    touchTimeout?: number
  ): Promise<void> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      const searchkey = `${pluginName}-${key}`;
      try {
        self.log.debug(await self.Redis.search_module_info(searchkey));
      } catch (exc) {
        let _keys: Array<{ type: string; name: string; as: string }> = [];

        for (let key of keys) {
          _keys.push({
            type: "TEXT",
            name: `$.${key.pathKey}`,
            as: key.as,
          });
        }

        _keys.push({
          type: "TAG",
          name: `$.${uniqueKeyPath}`,
          as: uniqueKeyAs,
        });
        _keys.push({
          type: "TAG",
          name: `$._pluginName`,
          as: "_pluginName",
        });
        _keys.push({
          type: "TAG",
          name: `$._skey`,
          as: "_skey",
        });
        self.log.debug(
          await self.Redis.search_module_create(searchkey, "JSON", _keys)
        );
        await self.set(`search-${searchkey}`, undefined, {
          touchTimeout: touchTimeout || 0,
          keys: _keys.map((x) => x.as).filter((x) => x[0] !== "_"),
        });
      }
    });
  }
  async doSearch<T extends CacheItem>(
    pluginName: string,
    key: string,
    data: any,
    touch: boolean = true
  ): Promise<Array<T>> {
    const self = this;
    const searchkey = `${pluginName}-${key}`;
    const config = (await self.get<{
      touchTimeout: number;
      keys: Array<string>;
    }>(`search-${searchkey}`, undefined)) || { touchTimeout: 0, keys: [] };

    let query: Array<string> = [];
    query.push(`@_pluginName:${pluginName}`);
    query.push(`@_skey:${key}`);
    for (let _key of config.keys) {
      if (Tools.isUndefined(data[_key])) continue;
      query.push(`@${_key}:${data[_key]}`);
    }

    let queryS = query.map((x) => `(${x})`).join("&");
    self.log.debug(queryS);
    let rConns = (await self.Redis.search_module_search(
      searchkey,
      queryS
    )) as any;
    if (rConns.length === undefined) return []; /// issue with query
    let responseCount = Number.parseInt(rConns.splice(0, 1)[0]);
    let rConnsParsed: Array<T> = [];
    for (let x = 0; x < responseCount; x++) {
      if (Tools.isNullOrUndefined(rConns[`${x}`])) continue;
      rConnsParsed.push(JSON.parse(rConns[`${x}`]["$"]));
    }
    if (touch && config.touchTimeout > 0)
      for (let rConn of rConnsParsed) {
        self.touch(`${searchkey}-${rConn.id}`, config.touchTimeout);
      }

    return rConnsParsed;
  }
  async addSearchData(
    pluginName: string,
    key: string,
    data: CacheItem | any
  ): Promise<void> {
    const self = this;
    const searchkey = `${pluginName}-${key}`;
    const config = (await self.get<{
      touchTimeout: number;
      keys: Array<string>;
    }>(`search-${searchkey}`, undefined)) || { touchTimeout: 0, keys: [] };
    if (Tools.isNullOrUndefined(data.id)) data.id = crypto.randomUUID();
    data._pluginName = pluginName;
    data._skey = key;
    await self.set(
      `${searchkey}-${data.id}`,
      undefined,
      data,
      config.touchTimeout
    );
  }
  async touchSearchData(
    pluginName: string,
    key: string,
    id: string
  ): Promise<void> {
    const searchkey = `${pluginName}-${key}`;
    const config = (await this.get<{
      touchTimeout: number;
      keys: Array<string>;
    }>(`search-${searchkey}`, undefined)) || { touchTimeout: 0, keys: [] };
    await this.touch(`${searchkey}-${id}`, config.touchTimeout);
  }
  async delSearchData(
    pluginName: string,
    key: string,
    id: string
  ): Promise<void> {
    const searchkey = `${pluginName}-${key}`;
    await this.del(`${searchkey}-${id}`);
  }
  async getSearchData<T extends CacheItem>(
    pluginName: string,
    key: string,
    id: string
  ): Promise<T | null> {
    const searchkey = `${pluginName}-${key}`;
    const config = (await this.get<{
      touchTimeout: number;
      keys: Array<string>;
    }>(`search-${searchkey}`, undefined)) || { touchTimeout: 0, keys: [] };
    const data = await this.get(`${searchkey}-${id}`);
    if (Tools.isNullOrUndefined(data)) return null;
    if (Tools.isString(data)) {
      if (data === "" || ["undefined", "null"].indexOf(data) >= 0) return null;
      if (config.touchTimeout > 0)
        await this.touch(`${searchkey}-${id}`, config.touchTimeout);
      return JSON.parse(data);
    }
    await this.touch(`${searchkey}-${id}`, config.touchTimeout);
    return data;
  }
}

export interface SearchKeys {
  pathKey: string;
  as: string;
}

export interface CacheItem {
  id: string;
}
