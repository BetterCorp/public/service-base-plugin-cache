import { RedisOptions } from 'ioredis';

export interface MyPluginConfig {
  db: RedisOptions;
}

export default (pluginName: string, existingPluginConfig: any): MyPluginConfig => {
  let newConfig: MyPluginConfig = {
    db: {
      host: '127.0.0.1',
      port: 6379,
      db: 0
    },
  };
  return newConfig;
};